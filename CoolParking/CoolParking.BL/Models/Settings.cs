﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
namespace CoolParking.BL.Models
{
    public class Settings
    {
        /// <summary>
        /// Початковий баланс Паркінгу
        /// </summary>
        public decimal ParkingBalance { get; set; }
        /// <summary>
        /// Місткість Паркінгу
        /// </summary>
        public int ParkingCapacity { get; set; }
        /// <summary>
        /// Період списання оплати, N-секунд
        /// </summary>
        public int PeriodPayment { get; set; }
        /// <summary>
        /// Період запису у лог, N-секунд
        /// </summary>
        public int PeriodLogSave { get; set; }
        /// <summary>
        /// Тарифи в залежності від Тр.засобу: Легкова
        /// </summary>
        public decimal TariffPassengerCar { get; set; }
        /// <summary>
        ///Тарифи в залежності від Тр.засобу: Вантажна
        /// <summary>
        public decimal TariffTruck { get; set; }
        /// <summary>
        ///Тарифи в залежності від Тр.засобу: Автобус
        /// <summary>
        public decimal TariffBus { get; set; }
        /// <summary>
        ///Тарифи в залежності від Тр.засобу: Мотоцикл
        /// <summary> 
        public decimal TariffMotorcycle { get; set; }
        /// <summary>
        /// Коефіцієнт штрафу.
        /// </summary>
        public decimal PenaltyCoefficient { get; set; }
        public Settings()
        {
            ParkingBalance = 0M;
            ParkingCapacity = 10;
            PeriodPayment = 5;
            PeriodLogSave = 60;
            TariffPassengerCar = 2M;
            TariffTruck = 5M;
            TariffBus = 3.5M;
            TariffMotorcycle = 1M;
            PenaltyCoefficient = 2.5M;
        }
    }
}