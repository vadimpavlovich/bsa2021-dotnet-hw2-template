﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        /// <summary>
        /// Parking balance
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// Vehicles on parking
        /// </summary>
        public List<Vehicle> Vehicles { get; set; }
        /// <summary>
        /// Parcking transactions
        /// </summary>
        public List<TransactionInfo> Transactions { get; set; }
        /// <summary>
        /// A reference to an instance of this Parking class
        /// </summary>
        private static Parking instance;
        /// <summary>
        /// Default parking constructor
        /// </summary>
        protected Parking(Settings settings) 
        { 
            this.Vehicles = new List<Vehicle>();
            this.Transactions = new List<TransactionInfo>();
            this.Balance = settings.ParkingBalance;
        }
        /// <summary>
        /// Get parking instance
        /// </summary>
        /// <returns>Existing parking class if it created previosly or new parking class if not</returns>
        public static Parking getInstance(Settings settings)
        {
            if (instance == null)
                instance = new Parking(settings);
            return instance;
        }
    }
}