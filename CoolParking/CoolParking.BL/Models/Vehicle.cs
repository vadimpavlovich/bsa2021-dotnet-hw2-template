﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;
namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        /// <summary>
        /// Vehicle unique id in correct format
        /// </summary>
        public string Id { get; }
        /// <summary>
        /// Type of vehicle
        /// </summary>
        public VehicleType VehicleType { get; }
        /// <summary>
        /// Get vehicle balance
        /// </summary>
        public decimal Balance { get { return _balance; } }
        /// <summary>
        /// Real balance variable
        /// </summary>
        private decimal _balance {get;set;} 
        /// <summary>
        /// Vehicle constructor
        /// </summary>
        /// <param name="Id">Unique vehicle id in correct format. Throw ArgumentException in case of invalid format</param>
        /// <param name="VehicleType">Type of vehicle</param>
        /// <param name="Balance">Start value of vehicle ballance</param>
        public Vehicle(string Id, VehicleType VehicleType, Decimal Balance)
        {
            if (!IsIdValid(Id))
                throw new ArgumentException("Id field is not valid.", "Id");
            if (Balance < 0)
                throw new ArgumentException("Start ballance cannot be negative.", "Balance"); 
            if (!Enum.IsDefined(typeof(VehicleType), VehicleType)) 
                throw new ArgumentException($"VehicleType {VehicleType} is incorrect.", "VehicleType");
            this.Id = Id;
            this.VehicleType = VehicleType;
            this._balance = Balance; 
        }

        /// <summary>
        /// Internal function to add funds to balance
        /// </summary>
        /// <param name="value">Sum of operation</param>
        internal void TopUpBalalnce(decimal value) 
        {
            _balance = _balance + value;
        }
        /// <summary>
        /// Internal function to write-off funds from balance
        /// </summary>
        internal void WriteOffBalalnce(decimal value)
        {
            _balance = _balance - value;
        }

        /// <summary>
        /// Generate random vehicle plate number
        /// </summary>
        /// <returns></returns>
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            string delimiter = "-";
            string result = GetLettersStr(2);
            result += delimiter;
            result += GetDigitsStr(4);
            result += delimiter;
            result += GetLettersStr(2);
            return result;
            string GetDigitsStr(int cnt)
            {
                string result = String.Empty;
                for (int i = 0; i < cnt; i++)
                {
                    int rndNum = random.Next(0, 9);
                    result += rndNum.ToString();
                }
                return result;
            }
            string GetLettersStr(int cnt)
            {
                string result = String.Empty;
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                for (int i = 0; i < cnt; i++)
                {
                    int rndNum = random.Next(0, 26);
                    result += chars[rndNum];
                }
                return result;
            }
        }

        /// <summary>
        /// Checks string for correct format 
        /// Example:
        /// In: AB-0123-CD Out: True 
        /// </summary>
        /// <param name="uuid">Input string</param>
        /// <returns></returns>
        public static bool IsIdValid(string uuid)
        {
            if (uuid == null)
                return false;
            string pattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";
            Regex rg = new Regex(pattern);
            return rg.IsMatch(uuid);
        }

        /// <summary>
        /// Overrided ToString method for prettyfy output Vehicele class
        /// </summary>
        /// <returns>Pretty Vehicele string record</returns>
        public override string ToString()
        {
            string result = String.Format("{0,-13}", Id);
            result += String.Format("  {0,-13}", VehicleType.ToString()); 
            if (_balance > 0)
                result += String.Format("   {0,-9}", _balance);
            else
                result += String.Format("  {0,-9}", _balance);
            result += Environment.NewLine;
            return result;
        }
    }
}