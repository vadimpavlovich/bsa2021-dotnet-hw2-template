﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        /// <summary>
        /// Transaction Sum
        /// </summary>
        public decimal Sum { get; set; }
        /// <summary>
        /// Car Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Balance before transaction
        /// </summary>
        public decimal BeforeBalance { get; set; }
        /// <summary>
        /// Balance after transaction
        /// </summary>
        public decimal AfterBalance { get; set; }
        /// <summary>
        /// Date and Time of transaction.
        /// Perhaps in a real project it would be more appropriate to use 
        /// the time in UTС, but here this is not necessary
        /// </summary>
        public DateTime Timestamp { get; set; } 
        /// <summary>
        /// Represents transaction type
        /// </summary>
        public TransactionTypes TransactionType { get; set; }
        /// <summary>
        /// TransactionInfo empty constructor
        /// </summary>
        public TransactionInfo() { }
        /// <summary>
        /// TransactionInfo internal constructor
        /// </summary>
        /// <param name="Id">Vehicle id</param>
        /// <param name="BeforeBalance">Vehicle balance before transaction</param>
        /// <param name="Sum">Sum of transaction</param>
        /// <param name="TransactionType">Type of transaction</param>
        internal TransactionInfo(string Id, decimal BeforeBalance, TransactionTypes TransactionType, decimal Sum) 
        {
            this.Id = Id;
            this.BeforeBalance = BeforeBalance;
            this.Sum = Sum;
            this.TransactionType = TransactionType;
            if (this.TransactionType == TransactionTypes.debit)
                this.AfterBalance = BeforeBalance + Sum;
            else
                this.AfterBalance = BeforeBalance - Sum;
            this.Timestamp = DateTime.Now; 
        }
        /// <summary>
        /// Overrided ToString method for prettyfy TransactionInfo class
        /// </summary>
        /// <returns>Pretty TransactionInfo string</returns>
        public override string ToString()
        {
            string result = Timestamp.ToString("dd-MM-yyyy HH:mm:ss.fff    ");
            result += String.Format("  {0,-13}", Id);
            if (BeforeBalance >= 0)
                result += String.Format("   {0,-9}", BeforeBalance);
            else
                result += String.Format("  {0,-9}", BeforeBalance);
            result += TransactionType == TransactionTypes.debit ? "+" : "-";
            result += String.Format("{0,-7}", Sum);
            if (AfterBalance >= 0)
                result += String.Format("   {0,-9}", AfterBalance);
            else
                result += String.Format("  {0,-9}", AfterBalance);
            result += Environment.NewLine;
            return result;
        }
        //It would be worth moving this enumeration into a separate file, 
        //but I'm not sure if this is provided for by the task
        /// <summary>
        /// Type of transaction
        /// </summary>
        public enum TransactionTypes
        {
            debit,
            credit
        }
    }
}