﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        /// <summary>
        /// Logger service
        /// </summary>
        private ILogService logger { get; }
        /// <summary>
        /// Timer service to withdrav funds
        /// </summary>
        private ITimerService withdrawTimer { get; set; }
        /// <summary>
        /// Timer service to save transaktions to log
        /// </summary>
        private ITimerService logTimer { get; set; }
        /// <summary>
        /// Global settings 
        /// </summary>
        private Settings settings { get; set; }
        /// <summary>
        /// Global parking class
        /// </summary>
        private Parking parking { get; set; }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService) 
        {
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            logTimer.Elapsed += LogTimerTick;
            withdrawTimer.Elapsed += WithdrawTimerTick;
            this.logger = logService;
            settings = new Settings();
            parking = Parking.getInstance(settings);
            this.logTimer.Start();
            this.withdrawTimer.Start();
         }
        /// <summary>
        /// Get current parking balance
        /// </summary>
        /// <returns>Decimal balance value</returns>        
        public decimal GetBalance()
        { 
            return parking.Balance; 
        }

        /// <summary>
        /// Get parking capacity
        /// </summary>
        /// <returns>Int capacity value</returns>
        public int GetCapacity() 
        { 
            return settings.ParkingCapacity; 
        }

        /// <summary>
        /// Get free places in parking
        /// </summary>
        /// <returns>Int32 free places value</returns>
        public int GetFreePlaces() 
        { 
            return settings.ParkingCapacity - parking.Vehicles.Count; 
        }

        /// <summary>
        /// Get list of vehicles on parking
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<Vehicle> GetVehicles() 
        {
            return parking.Vehicles.AsReadOnly();
        }

        /// <summary>
        /// Add vehicle on parking
        /// </summary>
        /// <param name="vehicle">Valid vehicle class</param>
        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null)
                throw new ArgumentException("Vehicle can not be null.", "vehicle");
            //Here you can put equal, because in theory we cannot have a negative 
            //number of free places, but it will be safer this way.
            if (GetFreePlaces() <= 0)
                throw new InvalidOperationException("Parking is full.");
            if (GetExistingVehicle(vehicle.Id) != null)
                throw new ArgumentException("Vehicle with such plate already exist.","vehicle.Id");
            parking.Vehicles.Add(vehicle);
        }

        /// <summary>
        /// Remove vehicle from parking in case of positive balance
        /// </summary>
        /// <param name="vehicleId">Valid vehicle Id string</param>
        public void RemoveVehicle(string vehicleId)
        {
            if (!Vehicle.IsIdValid(vehicleId))
                throw new ArgumentException("Such id have invalid format.", "vehicleId");
            Vehicle vehicle = GetExistingVehicle(vehicleId);
            if (vehicle == null)
                throw new ArgumentException("Such vehicle is not exist on parking.", "vehicleId");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("Vehicle has a negative balance.");
            parking.Vehicles.Remove(vehicle);
        }

        /// <summary>
        /// Top up vehicle balance according to its Id
        /// </summary>
        /// <param name="vehicleId">Id existing on parking vehicle</param>
        /// <param name="sum">Positive value of the top-up amount</param>
        public void TopUpVehicle(string vehicleId, decimal sum) 
        {
            if (!Vehicle.IsIdValid(vehicleId))
                throw new ArgumentException("Such id have invalid format.", "vehicleId");
            Vehicle vehicle = GetExistingVehicle(vehicleId);
            if (vehicle == null)
                throw new ArgumentException("Such vehicle is not exist on parking.", "vehicleId");
            if (sum < 0)
                throw new ArgumentException("Sum cannot be negative.", "sum");
            var prevBalance = vehicle.Balance;
            vehicle.TopUpBalalnce(sum);
        }

        /// <summary>
        /// Get last parking transactions
        /// </summary>
        /// <returns>Array of TransactionInfo</returns>
        public TransactionInfo[] GetLastParkingTransactions() 
        {
            return parking.Transactions.ToArray();
        }

        /// <summary>
        /// Read transactions records from log file
        /// </summary>
        /// <returns>Complete log file as string</returns>
        public string ReadFromLog()
        {
            return logger.Read(); 
        }

        /// <summary>
        /// Dispose resources
        /// </summary>
        public void Dispose()
        {
            parking.Vehicles.Clear();
            parking.Transactions.Clear();
            parking.Balance = settings.ParkingBalance;
            withdrawTimer?.Stop();
            logTimer?.Stop();
            withdrawTimer?.Dispose();
            logTimer?.Dispose();
        }
        
        /// <summary>
        /// Get existing vehicle from parking if its exist and null if not exist
        /// </summary>
        /// <param name="id">Valid vehicle id string</param>
        /// <returns>vehicle as Vehicle class</returns>
        private Vehicle GetExistingVehicle(string id) 
        {
            return parking.Vehicles.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Event handler for saving transactions to log
        /// </summary> 
        private void LogTimerTick(Object source, ElapsedEventArgs e)
        {
            string logInfo = string.Empty;
            foreach (var transaction in parking.Transactions)
            {
                logInfo += transaction.ToString(); 
            }
            parking.Transactions.Clear();
            //Here I would add a check for the presence of records and if they are not there, 
            //then I would not write them to the log. But the last test fails with this line, 
            //so I commented it out.
            //if (logInfo != string.Empty)
            logger.Write(logInfo);
        }

        /// <summary>
        /// Withdraw handler for debiting funds from vehicles
        /// </summary> 
        private void WithdrawTimerTick(Object source, ElapsedEventArgs e)
        {  
            foreach (Vehicle veh in parking.Vehicles)
            {
                decimal sum = 0;
                decimal before = veh.Balance;
                decimal rate = CalcRate(veh.VehicleType);
                if (veh.Balance >= rate) 
                {
                    sum = rate; 
                }
                else 
                {
                    if (veh.Balance > 0) 
                    {
                        decimal withPenalty = rate - veh.Balance;
                        decimal witoutPenalty = rate - withPenalty;
                        sum = witoutPenalty + withPenalty * settings.PenaltyCoefficient;
                    }
                    else 
                    {
                        sum = rate * settings.PenaltyCoefficient;
                    }
                }
                parking.Balance += sum;
                veh.WriteOffBalalnce(sum);
                TransactionInfo transaction = new TransactionInfo(veh.Id, before, TransactionInfo.TransactionTypes.credit, sum);
                parking.Transactions.Add(transaction);
            }
        }

        /// <summary>
        /// Calculate period rate according to VehicleType
        /// </summary> 
        private decimal CalcRate(VehicleType type) 
        {
            decimal rate = 0;
            switch (type)
            {
                case VehicleType.PassengerCar:
                    rate = settings.TariffPassengerCar;
                    break;
                case VehicleType.Truck:
                    rate = settings.TariffTruck;
                    break;
                case VehicleType.Bus:
                    rate = settings.TariffBus;
                    break;
                case VehicleType.Motorcycle:
                    rate = settings.TariffMotorcycle;
                    break;
                default:
                    throw new InvalidOperationException("Invalid VehicleType"); 
            }
            return rate; 
        }
    }
}