﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        /// <summary>
        /// Time interval after which you need to call the handler
        /// </summary>
        public double Interval { get; set; }
        /// <summary>
        /// External handler function
        /// </summary>
        public event ElapsedEventHandler Elapsed;

        private Timer _timer { get; set; }

        /// <summary>
        /// TimerService constructor
        /// </summary>
        /// <param name="interval">Time interval after which you need to call the handler. Value in seconds.</param>
        /// <param name="elapseEvent">External handler function</param>
        public TimerService(double Interval, ElapsedEventHandler Elapsed)
        {
            this.Interval = Interval;
            this.Elapsed = Elapsed;
            _timer = new Timer();
            _timer.Interval = Interval;
            _timer.Elapsed += Elapsed;
            _timer.AutoReset = true;
        }
        /// <summary>
        /// TimerService constructor without external ElapsedEventHandler
        /// </summary>
        /// <param name="Interval">Time interval after which you need to call the handler. Value in seconds.</param>
        public TimerService(double Interval)
        {
            this.Interval = Interval; 
            _timer = new Timer();
            _timer.Interval = Interval;
            _timer.Elapsed += ElapsedInternal;
            _timer.AutoReset = true;
        }
        /// <summary>
        /// Start timer with Interval
        /// </summary>
        public void Start()
        {
            _timer.Start();
        }
        /// <summary>
        /// Stop timer
        /// </summary>
        public void Stop()
        {
            _timer.Stop();
        }
        /// <summary>
        /// Dispose timer
        /// </summary>
        public void Dispose()
        {
            _timer.Dispose();
        }

        private void ElapsedInternal(Object source, ElapsedEventArgs e) 
        {
            Elapsed?.Invoke(source, e);
        }
    }
}