﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace CoolParking.BL.Services
{ 
    public class LogService : ILogService 
    {
        public string LogPath { get; }
        public LogService(string path) 
        {
            this.LogPath = path;
        }
        /// <summary>
        /// Append one line to LogFile
        /// </summary>
        /// <param name="logInfo"></param>
        public void Write(string logInfo)
        {
            List<string> strs = new List<string>();
            strs.Add(logInfo);
            File.AppendAllLines(LogPath, strs);
        }
        /// <summary>
        /// Read full LogFile   
        /// </summary>
        /// <returns>
        /// File content if file exist or InvalidOperationException if not exist
        /// </returns>
        public string Read() 
        {
            if (File.Exists(LogPath))
                return File.ReadAllText(LogPath);
            else
                throw new InvalidOperationException($"File not found. Path: {LogPath}");
        }
    }
}