﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService cloudParking;
        private readonly ILogger<ParkingController> logger;

        public ParkingController(ILogger<ParkingController> logger, IParkingService cloudParking)
        {
            this.logger = logger;
            this.cloudParking = cloudParking;
        }

        [HttpGet("freePlaces")]
        public IActionResult GetFreePlacesOnParking()
        {
            logger.LogInformation("GET /api/parking/freePlaces"); 
            return Ok(cloudParking.GetFreePlaces());
        }
        [HttpGet("capacity")]
        public IActionResult GetParkingCapacity()
        {
            logger.LogInformation("GET /api/parking/capacity"); 
            return Ok(cloudParking.GetCapacity());
        }
        [HttpGet("balance")]
        public IActionResult GetParkingBalance()
        {
            logger.LogInformation("GET /api/parking/balance"); 
            return Ok(cloudParking.GetBalance());
        }
    }
}
