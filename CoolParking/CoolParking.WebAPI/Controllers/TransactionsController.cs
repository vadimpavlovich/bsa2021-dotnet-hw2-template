﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService cloudParking;
        private readonly ILogger<TransactionsController> logger;

        public TransactionsController(ILogger<TransactionsController> logger, IParkingService cloudParking)
        {
            this.logger = logger;
            this.cloudParking = cloudParking;
        }

        [HttpGet("last")]
        public IActionResult GetLastParkingTransactions()
        {
            logger.LogInformation($"GET /api/transactions/last");
            return Ok(cloudParking.GetLastParkingTransactions());
        }

        [HttpGet("all")]
        public IActionResult GetAllParkingTransactions()
        {
            logger.LogInformation($"GET /api/transactions/all");
            try
            {
                string log = cloudParking.ReadFromLog();
                return Ok(log);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
            catch 
            {
                return StatusCode(500);
            }
        }

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicleBalanceOnParking(TopUpRequest request)
        { 
            logger.LogInformation($"PUT /api/transactions/topUpVehicle");
            if (!Vehicle.IsIdValid(request?.id))
                return BadRequest("The entered ID is not in the correct format.");
            if (request?.Sum < 0)
                return BadRequest("Sum cannot be negative.");
            var vehicles = cloudParking.GetVehicles();
            Vehicle vehicle = vehicles.Where(x => x.Id == request.id).FirstOrDefault();
            if (vehicle == null)
                return NotFound("There is no vehicle in the parking with the specified ID.");
            cloudParking.TopUpVehicle(request.id,request.Sum);
            return Ok(vehicle);
        } 
    }
}