﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService cloudParking;
        private readonly ILogger<VehiclesController> logger;

        public VehiclesController(ILogger<VehiclesController> logger, IParkingService cloudParking)
        {
            this.logger = logger;
            this.cloudParking = cloudParking;
        }

        [HttpGet]
        public IActionResult GetInfoAboutAllVehiclesInArrayFromParking()
        {
            logger.LogInformation($"GET /api/vehicles"); 
            return Ok(cloudParking.GetVehicles());
        }

        [HttpGet("{id}")]
        public IActionResult GetInfoAboutSpecifiedVehicleOnParking(string id)
        {
            logger.LogInformation($"GET /api/vehicles/{id}");
            if (!Vehicle.IsIdValid(id))
                return BadRequest("The entered ID is not in the correct format."); 
            var vehicles = cloudParking.GetVehicles();
            Vehicle vehicle = vehicles.Where(x => x.Id == id).FirstOrDefault();
            if (vehicle == null)
                return NotFound("There is no vehicle in the parking with the specified ID.");
            return Ok(vehicle);
        }
        [HttpPost] 
        public async Task<IActionResult> AddNewVehicleOnParking(VehicleRequest vehicle)
        {
            logger.LogInformation($"POST /api/vehicles"); 
            try
            {
                var veh = new Vehicle(vehicle.id, (VehicleType)vehicle.vehicleType, vehicle.balance);
                cloudParking.AddVehicle(veh);
                return Created("", veh); 
            }
            catch (Exception ex)
            {
                return BadRequest($"Error while adding car on parking: {ex.Message}"); 
            }
        }
        [HttpDelete("{id}")]
        public IActionResult RemoveExistingVehicleFromParking(string id)
        {
            logger.LogInformation($"DELETE /api/vehicles/{id}");
            if (!Vehicle.IsIdValid(id))
                return BadRequest("The entered ID is not in the correct format.");
            var vehicles = cloudParking.GetVehicles();
            Vehicle vehicle = vehicles.Where(x => x.Id == id).FirstOrDefault();
            if (vehicle == null)
                return NotFound("There is no vehicle in the parking with the specified ID.");
            try 
            {
                cloudParking.RemoveVehicle(id);
            }
            catch (Exception ex) 
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }
    }
}