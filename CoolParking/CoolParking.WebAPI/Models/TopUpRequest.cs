﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.Models
{
    public class TopUpRequest
    {  
        public string id { get; set; } 
        public decimal Sum { get; set; }
    }
}
