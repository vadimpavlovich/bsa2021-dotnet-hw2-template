﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class VehicleRequest
    {
        public string id { get; set; }
        public int vehicleType { get; set; }
        public decimal balance { get; set; }
    }
}
