﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services; 
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;

namespace CoolParking.WebAPI.Services
{
    public class CloudParkingService : IParkingService
    {
        /// <summary>
        /// Instance of ParkingService from BL
        /// </summary>
        private ParkingService parking { get; set; }
        /// <summary>
        /// Instance of LogService from BL
        /// </summary>
        private LogService logService { get; set; }

        /// <summary>
        /// Instance of ParkingService settings
        /// </summary>
        private Settings parkingSettings { get; set; }
 
        /// <summary>
        /// Default constructor for CloudParkingService
        /// </summary> 
        public CloudParkingService() 
        {
            parkingSettings = new Settings();
            var log_path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            logService = new LogService(log_path);
            var logTimer = new TimerService(parkingSettings.PeriodLogSave * 1000);
            var paymentTimer = new TimerService(parkingSettings.PeriodPayment * 1000);
            parking = new ParkingService(paymentTimer, logTimer, logService);
        }

        public decimal GetBalance()
        {
            return ((IParkingService)parking).GetBalance();
        }

        public int GetCapacity()
        {
            return ((IParkingService)parking).GetCapacity();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return ((IParkingService)parking).GetVehicles();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            ((IParkingService)parking).AddVehicle(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            ((IParkingService)parking).RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            ((IParkingService)parking).TopUpVehicle(vehicleId, sum);
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return ((IParkingService)parking).GetLastParkingTransactions();
        }

        public string ReadFromLog()
        {
            return ((IParkingService)parking).ReadFromLog();
        }

        public void Dispose()
        {
            ((System.IDisposable)parking).Dispose();
        }

        public int GetFreePlaces()
        {
            return ((IParkingService)parking).GetFreePlaces();
        } 
    }
}
