using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.IO;
using System.Reflection;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        static class MySingletonService
        {
            public static string MyVariable { get; set; }
        }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; } 
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary> 
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IParkingService, CloudParkingService>();
             
            services.AddControllers().AddJsonOptions(options =>
                options.JsonSerializerOptions.NumberHandling = System.Text.Json.Serialization.JsonNumberHandling.Strict
            ) ; 
        }
         
        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary> 
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
