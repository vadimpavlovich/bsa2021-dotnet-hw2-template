﻿using CoolParking.Interface.Models;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using System.Text;

namespace CoolParking.Interface.Services
{
    public class RequestService
    {
        private string ApiUrl { get; set; }
        private HttpClient client { get; set; }
        public RequestService(string ApiUrl)
        {
            this.ApiUrl = ApiUrl;
            client = new HttpClient();
        }
        public RequestResult Get(string path)
        {
            RequestResult result = new RequestResult();
            try
            {
                var webRequest = new HttpRequestMessage(HttpMethod.Get, ApiUrl + path);
                var response = client.Send(webRequest);
                using var reader = new StreamReader(response.Content.ReadAsStream());
                result.IsSuccess = response.IsSuccessStatusCode;
                result.Responce = reader.ReadToEnd();
            }
            catch
            {
                result.IsSuccess = false;
                result.Responce = string.Empty;
            }
            return result;
        }
        public RequestResult Post(string path, object data)
        {
            RequestResult result = new RequestResult();
            try
            {
                var webRequest = new HttpRequestMessage(HttpMethod.Post, ApiUrl + path)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")
                };
                var response = client.Send(webRequest);
                using var reader = new StreamReader(response.Content.ReadAsStream());
                result.IsSuccess = response.IsSuccessStatusCode;
                result.Responce = reader.ReadToEnd();
                return result;
            }
            catch
            {
                result.IsSuccess = false;
                result.Responce = string.Empty;
            }
            return result;
        }
        public RequestResult Put(string path, object data)
        {
            RequestResult result = new RequestResult();
            try
            {
                var webRequest = new HttpRequestMessage(HttpMethod.Put, ApiUrl + path)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")
                };
                var response = client.Send(webRequest);
                using var reader = new StreamReader(response.Content.ReadAsStream());
                result.IsSuccess = response.IsSuccessStatusCode;
                result.Responce = reader.ReadToEnd();
                return result;
            }
            catch
            {
                result.IsSuccess = false;
                result.Responce = string.Empty;
            }
            return result;
        }
        public RequestResult Delete(string path)
        {
            RequestResult result = new RequestResult();
            try
            {
                var webRequest = new HttpRequestMessage(HttpMethod.Delete, ApiUrl + path);
                var response = client.Send(webRequest);
                using var reader = new StreamReader(response.Content.ReadAsStream());
                result.IsSuccess = response.IsSuccessStatusCode;
                result.Responce = reader.ReadToEnd();
                var s = response.ReasonPhrase;
                return result;
            }
            catch
            {
                result.IsSuccess = false;
                result.Responce = string.Empty;
            }
            return result;
        }
    }
}
