﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.Interface.Menu;
using CoolParking.Interface.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.Interface.Services
{
    public class CloudParkingService 
    {  
        private RequestService requester { get; set; }
        public CloudParkingService(string ApiPath) 
        {
            requester = new RequestService(ApiPath);
        } 
        public void GetParkingBalanceAndPrintResult()
        {
            string path = @"parking/balance";
            var result = requester.Get(path);
            Console.Clear();
            if (result.IsSuccess) 
                ColorConsole.GreenColorOutput($"Parking Ballance is {result.Responce}. "); 
            else 
                ColorConsole.RedColorOutput("Api request error." + result.Responce);  
        }
        public void GetParkingFreePlacesAndPrintResult()
        { 
            var result1 = requester.Get(@"parking/capacity");
            var result2 = requester.Get(@"parking/freePlaces");
            Console.Clear();
            if (result1.IsSuccess && result2.IsSuccess)
                ColorConsole.GreenColorOutput($"Free parking spaces {result2.Responce} out of {result1.Responce}."); 
            else
                ColorConsole.RedColorOutput("Api request error." + result1.Responce + " "+ result2.Responce);
        } 
        public void GetLastTimeProfitAndPrintResult()
        {
            var result = requester.Get(@"transactions/last");
            Console.Clear();
            if (result.IsSuccess)
            { 
                var Transactions = JsonConvert.DeserializeObject<List<TransactionInfo>>(result.Responce);
                ColorConsole.GreenColorOutput($"Parking las time profit is {Transactions.Sum(tr => tr.Sum)}. "); 
            }
            else
                ColorConsole.RedColorOutput("Api request error." + result.Responce);
        }
        public void GetLastParkingTransactionsAndPrintResult() 
        {
            var result = requester.Get(@"transactions/last");
            Console.Clear();
            if (result.IsSuccess)
            {
                var Transactions = JsonConvert.DeserializeObject<List<TransactionInfo>>(result.Responce);
                if (Transactions.Count() > 0)
                {
                    ColorConsole.GreenColorOutput("Last transactions: ");
                    ColorConsole.GreenColorOutput(GetTransactionsHeader());
                    foreach (var item in Transactions)
                    {
                        Console.Write(item.ToString());
                    }
                } 
                else
                {
                    ColorConsole.GreenColorOutput("There hasn't been a transaction yet.");
                }
            }
            else
                ColorConsole.RedColorOutput("Api request error. "+result.Responce);
        }
        public void GetAllParkingTransactionsAndPrintResult() 
        { 
            var result = requester.Get(@"transactions/all");
            Console.Clear();
            if (result.IsSuccess)
            {
                ColorConsole.GreenColorOutput(GetTransactionsHeader());
                Console.WriteLine(result.Responce);
            }
            else
            {
                ColorConsole.RedColorOutput("Api request error. " + result.Responce);
            } 
        }
        public void GetAllParkingVehiclesAndPrintResult() 
        {
            Console.Clear();
            var result = requester.Get(@"vehicles");
            if (result.IsSuccess)
            {
                var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(result.Responce);
                ColorConsole.GreenColorOutput("Vehicles on parking: ");
                if (vehicles.Count > 0)
                {
                    ColorConsole.GreenColorOutput(GetVehiclesHeader());
                    foreach (var item in vehicles)
                    {
                        Console.Write(item.ToString());
                    }
                }
                else
                {
                    ColorConsole.GreenColorOutput("There are no vehicles on parking.");
                }
            }
            else
                ColorConsole.RedColorOutput("Api request error. " + result.Responce); 
        } 
        public void AddVehicleOnParkingAndPrintResult(Vehicle vehicle) 
        {
            string path = @"vehicles";
            var result = requester.Post(path, vehicle);
            Console.Clear();
            if (result.IsSuccess)
                ColorConsole.GreenColorOutput("New vehicle successfully added on parking. ");
            else
                ColorConsole.RedColorOutput("Api request error." + result.Responce);
        }
        public void DeleteVehicleFromParkingAndPrintResult(string vehicleId)
        {
            string path = $@"vehicles/{vehicleId}";
            var result = requester.Delete(path);
            Console.Clear();
            if (result.IsSuccess)
                ColorConsole.GreenColorOutput("New vehicle successfully removed from parking. ");
            else
                ColorConsole.RedColorOutput("Api request error. " + result.Responce);
        }
        public void TopUpVehicleBalanceAndPrintResult(string id, decimal balance) 
        {
            string path = $@"transactions/topUpVehicle";
            TopUpRequest rec = new TopUpRequest(id,balance);
            var result = requester.Put(path,rec);
            Console.Clear();
            if (result.IsSuccess)
                ColorConsole.GreenColorOutput("Vehicle balance successfully top up. ");
            else
                ColorConsole.RedColorOutput("Api request error. " + result.Responce); 
        }

        private string GetTransactionsHeader()
        {
            return "   Date        Time              Id      Bal.before    Sum     Account balance";
        }
        private string GetVehiclesHeader()
        {
            return "    Id           Type        Balance";
        }
    }
}
