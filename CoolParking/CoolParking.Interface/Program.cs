﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System; 
using System.IO;
using System.Reflection;
using CoolParking.Interface.Menu;
using System.Timers;
using CoolParking.Interface.Services;

namespace CoolParking.Interface
{
    class Program
    {
        /// <summary>
        /// Global ParkingService object
        /// </summary>
        internal static CloudParkingService parking { get; set; }
        private const string api = @"http://localhost:51465/api/";
        static void Main(string[] args)
        {
            parking = new CloudParkingService(api);
            Console.WriteLine($"Hello, {Environment.UserName}!");
            IState startState = new MainMenu();
            while (startState != null) startState = startState.RunState();
            Console.Clear();
            Console.WriteLine($"Bye, {Environment.UserName}!"); 
        }  
    } 
}
