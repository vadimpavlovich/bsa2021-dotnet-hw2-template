﻿ 
namespace CoolParking.Interface.Models
{
    public class RequestResult
    {
        public bool IsSuccess { get; set; }
        public string Responce { get; set; }

        public RequestResult() { }
        public RequestResult(bool IsSuccess, string Responce)
        {
            this.IsSuccess = IsSuccess;
            this.Responce = Responce;
        }
    }
}
