﻿using Newtonsoft.Json; 

namespace CoolParking.Interface.Models
{
    public class TopUpRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        public decimal Sum { get; set; }

        public TopUpRequest(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }
    }
}
