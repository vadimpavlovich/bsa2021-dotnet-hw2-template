﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Menu
{
    /// <summary>
    /// Menu item class
    /// </summary>
    public class MenuItem
    {
        public string Text { get; set; }
    } 
    /// <summary>
    /// Class for managing menu state and navigation
    /// </summary>
    public abstract class MenuState : IState
    {
        protected abstract Dictionary<int, MenuItem> Menus { get; }

        protected virtual void ShowMenu()
        {
            foreach (var m in Menus)
                Console.WriteLine($"{m.Key} - {m.Value.Text}");
        }

        protected virtual KeyValuePair<int, MenuItem> ReadOption()
        {
            Console.WriteLine("Please, select option:");
            ShowMenu();

            var str = Console.ReadLine();
            int answerId = 0;

            if (int.TryParse(str, out answerId))
            {
                if (!Menus.ContainsKey(answerId))
                {
                    Console.Clear();
                    Console.WriteLine("Selected item not exists.");
                    return ReadOption();
                }
                return new KeyValuePair<int, MenuItem>(answerId, Menus[answerId]);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Selected item not a number.");
                return ReadOption();
            }
        }

        public virtual IState RunState()
        {
            var option = ReadOption();
            return NextState(option);
        }

        protected abstract IState NextState(KeyValuePair<int, MenuItem> selectedMenu);
    }
}