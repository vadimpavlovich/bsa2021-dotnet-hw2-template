﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Menu
{
    /// <summary>
    /// Remove Vehicle from parking visual interface
    /// </summary>
    public class RemoveVehicleMenu : IState
    {
        public IState RunState()
        {
            ColorConsole.GreenColorOutput("Remove car from parking (type quit to return in main menu): ");
            bool valueValid = false;
            while (!valueValid)
            {
                ColorConsole.GreenColorOutput("Enter Id in the correct format (e.g. AA-0001-AA): ");
                var input = Console.ReadLine();
                if (isQuit(input))
                    return new MainMenu();
                try
                {  
                    Program.parking.DeleteVehicleFromParkingAndPrintResult(input);
                    valueValid = true; 
                }
                catch (Exception ex)
                {
                    ColorConsole.RedColorOutput(ex.Message);
                } 
            } 
            return new MainMenu();
            bool isQuit(string input)
            {
                if (input == "quit")
                {
                    Console.Clear();
                    return true;
                }
                return false;
            }
        }
    }
}
