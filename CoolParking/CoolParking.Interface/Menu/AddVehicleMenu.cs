﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Menu
{
    /// <summary>
    /// Add vehicle visual interface
    /// </summary>
    public class AddVehicleMenu : IState
    {
        public IState RunState()
        {

            string id = string.Empty;
            decimal balance = 0;
            VehicleType type = VehicleType.PassengerCar;
            ColorConsole.GreenColorOutput("Adding new car (type quit to return in main menu): ");
            bool valueValid = false;
            while (!valueValid)
            {
                ColorConsole.GreenColorOutput("Enter Id in the correct format (e.g. AA-0001-AA): ");
                var input = Console.ReadLine();
                if (isQuit(input))
                    return new MainMenu();
                if (Vehicle.IsIdValid(input))
                {
                    id = input;
                    valueValid = true;
                }
                else
                {
                    ColorConsole.RedColorOutput($"Id = {input} - IS INCORRECT VALUE! Please, ");
                }
            }
            valueValid = false;
            while (!valueValid)
            {
                ColorConsole.GreenColorOutput("Enter correct vhicle type (0-PassengerCar; 1-Truck; 2-Bus; 3-Motorcycle):");
                var input = Console.ReadLine();
                if (isQuit(input))
                    return new MainMenu();
                int type_input = 0;
                var correct = Int32.TryParse(input, out type_input);
                if (correct && (type_input is 0 or 1 or 2 or 3))
                {
                    type = (VehicleType)type_input;
                    valueValid = true;
                }
                else
                {
                    ColorConsole.RedColorOutput($"Type = {input} - IS INCORRECT VALUE! Please, ");
                }
            }
            valueValid = false;
            while (!valueValid)
            {
                ColorConsole.GreenColorOutput("Enter vehicle balance:");
                var input = Console.ReadLine();
                if (isQuit(input))
                    return new MainMenu();
                decimal balance_input = 0;
                var correct = Decimal.TryParse(input, out balance_input);
                if (correct && balance_input >= 0)
                {
                    balance = balance_input;
                    valueValid = true;
                }
                else
                {
                    ColorConsole.RedColorOutput($"Balance = {input} - IS INCORRECT VALUE! Please, ");
                }
            }
            Vehicle veh = new Vehicle(id, type, balance);
            Program.parking.AddVehicleOnParkingAndPrintResult(veh); 
            return new MainMenu();
            bool isQuit(string input)
            {
                if (input == "quit")
                {
                    Console.Clear();
                    return true;
                }
                return false;
            }
        }
    }
}
