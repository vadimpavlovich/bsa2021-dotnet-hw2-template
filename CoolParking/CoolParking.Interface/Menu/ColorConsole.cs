﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Menu
{
    public static class ColorConsole
    { 
        /// <summary>
        /// Write message to console with green color
        /// </summary>
        /// <param name="input">The text to be printed to the console</param>
        public static void GreenColorOutput(string input) 
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;  
            Console.WriteLine(input);
            Console.ResetColor(); 
        } 
        /// <summary>
        /// Write message to console with red color
        /// </summary>
        /// <param name="input">The text to be printed to the console</param>
        public static void RedColorOutput(string input)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed; 
            Console.WriteLine(input);
            Console.ResetColor();
        }


    }
}
