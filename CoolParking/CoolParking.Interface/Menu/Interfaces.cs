﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Menu
{
    /// <summary>
    /// State interface
    /// </summary>
    public interface IState
    {
        IState RunState();
    }
}
