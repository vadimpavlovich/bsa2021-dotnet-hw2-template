﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Menu
{
    /// <summary>
    /// Top up vehicle balance visual interface
    /// </summary>
    public class TopUpVehicleMenu : IState
    {
        public IState RunState()
        {
            string id = string.Empty;
            decimal balance = 0;
            ColorConsole.GreenColorOutput("Top up vehicle balance (type quit to return in main menu): ");
            bool valueValid = false;
            while (!valueValid)
            {
                ColorConsole.GreenColorOutput("Enter Id in the correct format (e.g. AA-0001-AA): ");
                var input = Console.ReadLine();
                if (isQuit(input))
                    return new MainMenu();
                if (Vehicle.IsIdValid(input))
                {
                    id = input;
                    valueValid = true;
                }
                else
                {
                    ColorConsole.RedColorOutput($"Id = {input} - IS INCORRECT VALUE! Please, ");
                }
            }
            valueValid = false;
            while (!valueValid)
            {
                ColorConsole.GreenColorOutput("Enter sum of top up:");
                var input = Console.ReadLine();
                if (isQuit(input))
                    return new MainMenu();
                decimal balance_input = 0;
                var correct = Decimal.TryParse(input, out balance_input);
                if (correct && balance_input >= 0)
                {
                    balance = balance_input;
                    valueValid = true;
                }
                else
                {
                    ColorConsole.RedColorOutput($"Sum = {input} - IS INCORRECT VALUE! Please, ");
                }
            } 
            Program.parking.TopUpVehicleBalanceAndPrintResult(id, balance); 
            return new MainMenu();
            bool isQuit(string input)
            {
                if (input == "quit")
                {
                    Console.Clear();
                    return true;
                }
                return false;
            }
        }
    }
}
