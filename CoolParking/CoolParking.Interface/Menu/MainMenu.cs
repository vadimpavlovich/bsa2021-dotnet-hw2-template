﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Interface.Menu
{
    /// <summary>
    /// Main menu visual interface
    /// </summary>
    public class MainMenu : MenuState
    {
        private Dictionary<int, MenuItem> _menus = new Dictionary<int, MenuItem>() {
                {1,  new MenuItem(){Text = "Show parking balance"}},
                {2,  new MenuItem(){Text = "Show last time profit"}},
                {3,  new MenuItem(){Text = "Show free spaces"}},
                {4,  new MenuItem(){Text = "Show actual transactions"}},
                {5,  new MenuItem(){Text = "Show transactions history"}},
                {6,  new MenuItem(){Text = "Show vehicles on parking"}},
                {7,  new MenuItem(){Text = "Add vehicle on parking"}},
                {8,  new MenuItem(){Text = "Remove vehicle from parking"}},
                {9,  new MenuItem(){Text = "Top up vehicle balance"}},
                {10, new MenuItem(){Text = "Exit"}},
            };
        protected override Dictionary<int, MenuItem> Menus => _menus;

        protected override IState NextState(KeyValuePair<int, MenuItem> selectedMenu)
        {
            switch (selectedMenu.Key)
            {
                case 1:
                    Program.parking.GetParkingBalanceAndPrintResult();
                    return this;
                case 2:
                    Program.parking.GetLastTimeProfitAndPrintResult();
                    return this;
                case 3:
                    Program.parking.GetParkingFreePlacesAndPrintResult();
                    return this;
                case 4: 
                    Program.parking.GetLastParkingTransactionsAndPrintResult();
                    return this;
                case 5:
                    Program.parking.GetAllParkingTransactionsAndPrintResult();
                    return this;
                case 6:
                    Program.parking.GetAllParkingVehiclesAndPrintResult();
                    return this;
                case 7:
                    Console.Clear();
                    return new AddVehicleMenu();
                case 8:
                    Console.Clear();
                    return new RemoveVehicleMenu();
                case 9:
                    Console.Clear();
                    return new TopUpVehicleMenu();
                case 10:
                    Console.Clear();
                    return null;
                default:
                    Console.Clear();
                    return this;
            }
        }

      
    }
}
